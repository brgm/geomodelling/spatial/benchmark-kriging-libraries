# -*- coding: utf-8 -*-
from time import perf_counter

import numpy as np
import pandas as pd

from benchmark_geostat import ALL_BENCHES, RESULT_DIR, load_data_set

params = {
    "nb_realizations": 10,
    "range": 80000,
    "sill": 14000,
}
rtol = 1e-8  # Relative tolerance to compare estimation results between the libraries
nb_samples = 200

# Input samples
samples = load_data_set()
samples = samples[:nb_samples]
# Output targets
x0, y0 = -180000, -120000
nx, ny = 361, 241
step = 1000
xn = x0 + nx * step
yn = y0 + ny * step

all_targets = np.empty((step, step, 2), dtype=np.float64)
x = np.linspace(x0, xn, step)
all_targets[..., 0] = x[:, np.newaxis]
y = np.linspace(y0, yn, step)
all_targets[..., 1] = y[np.newaxis, :]
all_targets = all_targets.reshape((-1, 2))

columns = [c for name in ALL_BENCHES for c in (f"{name} (mean)", f"{name} (std)")]
results = pd.DataFrame(columns=columns)


for exponent in range(3, 6):
    nb_targets = 10**exponent
    print("Number of output targets:", nb_targets)
    targets = all_targets[:nb_targets]
    result = {}
    values = []  # Check that all libraries return similar estimations

    # Profile each library
    for name, benchmark in ALL_BENCHES.items():
        print(f"{name}: ", end="", flush=True)
        t0 = perf_counter()
        mean, std, z = benchmark(samples, targets, params)
        t1 = perf_counter()
        # Check plausibility of the result, do not remove!
        mtime = params["nb_realizations"] * mean
        ttime = t1 - t0
        print(
            f"{(len(name) + 2)* ' '}"
            f"Measured time: {mtime:.2e} s over total time {ttime:.2e}"
            f" --> {(10_000 * mtime / ttime) // 100} %"
        )
        result.update({f"{name} (mean)": mean, f"{name} (std)": std})
        values.append((name, z))

    # Compare estimation results
    for i1, (n1, z1) in enumerate(values):
        for i2 in range(i1 + 1, len(values)):
            n2, z2 = values[i2]
            if not np.allclose(z1, z2, rtol=rtol, atol=0):
                print(f"{n1} != {n2}, with a relative tolerance of {rtol}")

    # Update the output table
    results.loc[str(nb_targets)] = result
    print()

# sep=";" => for opening with Excel french version...
results.to_csv(RESULT_DIR / "bench_targets_evaluation.csv", sep=";")
