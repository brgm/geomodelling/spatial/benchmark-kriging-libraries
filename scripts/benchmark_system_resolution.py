# -*- coding: utf-8 -*-
from time import perf_counter

import numpy as np
import pandas as pd

from benchmark_geostat import ALL_BENCHES, RESULT_DIR, create_more_data, load_data_set

params = {
    "nb_realizations": 100,
    "range": 80000,
    "sill": 14000,
}
rtol = 1e-8  # Relative tolerance to compare estimation results between the libraries

# Input samples
samples_full = load_data_set()
samples_full = create_more_data(samples_full, params, nb_total_data=2000)
# Output targets (only one)
targets = np.mean(samples_full[:, :2], axis=0, keepdims=True)
assert targets.shape == (1, 2)


columns = [c for name in ALL_BENCHES for c in (f"{name} (mean)", f"{name} (std)")]
results = pd.DataFrame(columns=columns)

for nb_samples in (20, 50, 100, 150, 200, 350, 500, 750, 1000, 1500, 2000):
    print("Number of input samples:", nb_samples)
    samples = samples_full[:nb_samples]
    result = {}
    values = []  # Check that all libraries return similar estimations

    # Profile each library
    for name, benchmark in ALL_BENCHES.items():
        print(f"{name}: ", end="", flush=True)
        t0 = perf_counter()
        mean, std, z = benchmark(samples, targets, params)
        t1 = perf_counter()
        # Check plausibility of the result, do not remove!
        mtime = params["nb_realizations"] * mean
        ttime = t1 - t0
        print(
            f"{(len(name) + 2)* ' '}"
            f"Measured time: {mtime:.2e} s over total time {ttime:.2e}"
            f" --> {(10_000 * mtime / ttime) // 100} %"
        )
        result.update({f"{name} (mean)": mean, f"{name} (std)": std})
        values.append((name, z))

    # Compare estimation results
    for i1, (n1, z1) in enumerate(values):
        for i2 in range(i1 + 1, len(values)):
            n2, z2 = values[i2]
            if not np.allclose(z1, z2, rtol=rtol, atol=0):
                print(f"{n1} != {n2}, with a relative tolerance of {rtol}")

    # Update the output table
    results.loc[str(nb_samples)] = result
    print()

# sep=";" => for opening with Excel french version...
results.to_csv(RESULT_DIR / "bench_kriging_system_resolution.csv", sep=";")
